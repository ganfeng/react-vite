import React from "react";
import {
  HashRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import LoginPage from "./views/login";
import MainPage from "./views/main";
import './App.less';
export default function App() {
  return (
    <Router>
      <div className="main-container">
        <Switch>
          <Route path="/main">
            <MainPage />
          </Route>
          <Route path="/login">
            <LoginPage></LoginPage>
          </Route>
          <Route render={() => <div> 404页面 </div>} />
        </Switch>
      </div>
    </Router>
  );
}

