import axios from 'axios'
// @ts-ignore
const isDev = process.env.NODE_ENV !== 'production'
const BASEURL = isDev ? '' : ''
const config = { timeout: 120000 }
export default (api, data, callback, errorCallback, notice = true) => {
  let url = BASEURL + api
  let param = {}
  let token = localStorage.getItem('token');
  Object.assign(param, data);
  axios.defaults.headers.common['Access-Token'] = token
  isDev && console.log(param)

  return axios.post(url, param, config)
    .then((response) => {
      let { code, message, data, status } = response.data
      if (code === 1) {
        callback && typeof callback === 'function' && callback(data)
      } else {
        errorCallback && typeof errorCallback === 'function' && errorCallback(code, message, data)
      }
    })
    .catch((error) => {
      isDev && console.log(error)
    })
}