export const LOGIN = {
  requestLogin: '/web/login',
  getCaptcha: '/web/getCaptcha'
}
export const USER = {
  getUserList: '/web/getUserList',
  addUser: '/web/addUser',
  deleteUser: '/web/deleteUser',
  modifyUser: '/web/modifyUser',
  getPayerList: '/web/getPayerList'
}
export const BILL = {
  getBillList: '/web/getBillList',
  addBill: '/web/addBill',
  deleteBill: '/web/deleteBill',
  modifyBill: '/web/modifyBill',
  getBillDetail: '/web/getBillDetail'
}