import React, { useState, useEffect } from "react";
import { Form, Input, Button, Row, Col } from 'antd';
import { UserOutlined, LockOutlined, CodeOutlined } from '@ant-design/icons';
import useUserModel from "../../model/useUserModel";
import 'antd/dist/antd.less';
import http from '../../http/axios';
import { LOGIN } from "../../http";
import md5 from "js-md5";
import './index.less';
const layout = {
  wrapperCol: {
    span: 24,
  },
};
export default function LoginPage() {
  const user = useUserModel();
  const [info, setInfo] = useState('init info');
  // useEffect(() => {
  //   changeCode();
  // })
  const onFinish = (values) => {
    console.log('Success:', values);
    user.login(values.username)
    return;
    values.password = md5(values.password);
    http(LOGIN.requestLogin, values).then(res => {
      console.log(res);
    })
  };
  if (user.userInfo) {
    return (
      <Button type="danger" onClick={user.logout}>
        Logout
      </Button>
    );
  }
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  const changeCode = () => {
    http(LOGIN.getCaptcha, null, res => {
      setInfo(res.code)
      localStorage.setItem('token', res.token)
    }, error => {
      console.log(error);
    })
  }
  return (
    <div className="login-container">
      <div className="login-box">
        <h1>xx</h1>
        <Form
          {...layout}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: '请输入用户名!',
              },
            ]}
          >
            <Input placeholder="请输入用户名" prefix={<UserOutlined />} />
          </Form.Item>

          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: '请输入密码!',
              },
            ]}
          >
            <Input.Password placeholder="请输入密码" prefix={<LockOutlined />} />
          </Form.Item>
          <Form.Item
            name="code"
            rules={[
              {
                required: true,
                message: '请输入验证码!',
              },
            ]}>
            <Row gutter={8}>
              <Col span={12}>
                <Form.Item >
                  <Input placeholder="请输入验证码" prefix={<CodeOutlined />} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item>
                  <div dangerouslySetInnerHTML={{ __html: info }}></div>
                </Form.Item></Col></Row>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" block>
              登录
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}