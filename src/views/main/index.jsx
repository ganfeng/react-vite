import React from "react";
import {
  HashRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { Layout, Breadcrumb } from 'antd';
const { Content, Sider } = Layout;
import HeaderPage from "./header";
import MenuPage from "./menu";
import NameTablePage from "./name_table";
import X6 from './x6';
import G6Demo from './g6';
export default function MainPage() {
  return (
    <Router>
      <Layout>
        <HeaderPage></HeaderPage>
        <Layout>
          <Sider width={200} className="site-layout-background">
            <MenuPage></MenuPage>
          </Sider>
          <Layout style={{ padding: '0 24px 24px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item>List</Breadcrumb.Item>
              <Breadcrumb.Item>App</Breadcrumb.Item>
            </Breadcrumb>
            <Content
              className="site-layout-background"
              style={{
                padding: 24,
                margin: 0,
                minHeight: 280,
              }}
            >
              <Switch>
                <Route path="/main/about">
                  <About />
                </Route>
                <Route path="/main/users">
                  <Users />
                </Route>
                <Route path="/main/home">
                  <Home></Home>
                </Route>
                <Route path="/main/nameTable">
                  <NameTablePage></NameTablePage>
                </Route>
                <Route path="/main/x6">
                  <X6></X6>
                </Route>
                <Route path="/main/g6">
                  <G6Demo></G6Demo>
                </Route>
                <Route render={() => <div> 404页面 </div>} />
              </Switch>
            </Content>
          </Layout>
        </Layout>
      </Layout>
    </Router>
  );
}
function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}