import React from "react";
import { Menu, Button } from 'antd';
import {
  HashRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import {
	AppstoreOutlined,
	MenuUnfoldOutlined,
	MenuFoldOutlined,
	PieChartOutlined,
	DesktopOutlined,
	ContainerOutlined,
	MailOutlined,
} from '@ant-design/icons';

const { SubMenu } = Menu;

export default class MenuPage extends React.Component {
	state = {
		collapsed: false,
	};

	toggleCollapsed = () => {
		this.setState({
			collapsed: !this.state.collapsed,
		});
	};

	render() {
		return (
			<div style={{ width: 200 }}>
				<Button type="primary" onClick={this.toggleCollapsed} style={{ marginBottom: 16 }}>
					{React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
				</Button>
				<Menu
					defaultSelectedKeys={['home']}
					mode="inline"
					inlineCollapsed={this.state.collapsed}
				>
					<Menu.Item key="home" icon={<PieChartOutlined />}>
						<Link to="/main/home">Home</Link>
					</Menu.Item>
					<Menu.Item key="users2" icon={<DesktopOutlined />}>
						<Link to="/main/users">Users</Link>
					</Menu.Item>
					<Menu.Item key="about" icon={<ContainerOutlined />}>
						<Link to="/main/about">about</Link>
					</Menu.Item>
					<Menu.Item key="nameTable" icon={<ContainerOutlined />}>
						<Link to="/main/nameTable">nameTable</Link>
					</Menu.Item>
					<Menu.Item key="x6" icon={<ContainerOutlined />}>
						<Link to="/main/x6">x6</Link>
					</Menu.Item>
					<Menu.Item key="g6" icon={<ContainerOutlined />}>
						<Link to="/main/g6">g6</Link>
					</Menu.Item>
				</Menu>
			</div>
		);
	}
}