import React, { useEffect } from "react";
import { Graph, Addon } from '@antv/x6';
import './index.less'
import { Button } from 'antd';
import _ from 'lodash';
export default function X6() {
  function renderHtml(id) {
    let tempData = _.find(data.nodes, { id }).extraData;
    return `<div class="condition-box">
    <div class="condition-box-image">v</div>
    <div class="condition-box-name">${tempData.name}</div>
    <div class="condition-box-operations">x</div>
  </div>`
  }
  let graph;
  const data = {
    // 节点
    nodes: [
      {
        id: 'node1', // String，可选，节点的唯一标识
        x: 40,       // Number，必选，节点位置的 x 值
        y: 40,       // Number，必选，节点位置的 y 值
        width: 100,   // Number，可选，节点大小的 width 值
        height: 100,  // Number，可选，节点大小的 height 值
        shape: 'html',
        extraData: {
          name: 'hello'
        },
        html(e) {
          let tempData = _.find(data.nodes, { id: e.id });
          return renderHtml(e.id)
        },
      },
      {
        id: 'node2', // String，节点的唯一标识
        x: 160,      // Number，必选，节点位置的 x 值
        y: 40,      // Number，必选，节点位置的 y 值
        width: 100,   // Number，可选，节点大小的 width 值
        height: 100,  // Number，可选，节点大小的 height 值
        shape: 'html',
        extraData: {
          name: 'world'
        },
        html: (e) => {
          return renderHtml(e.id)
        },
      },
    ],
    // 边
    edges: [
      {
        source: 'node1', // String，必须，起始节点 id
        target: 'node2', // String，必须，目标节点 id
      },
    ],
  };

  useEffect(() => {
    graph = new Graph({
      container: document.getElementById('container'),
      width: 800,
      height: 600,
      background: {
        color: '#fffbe6', // 设置画布背景颜色
      },
      grid: {
        size: 10,      // 网格大小 10px
        visible: true, // 渲染网格背景
      },
    });
    const stencil = new Addon.Stencil({ target: graph })
    // const $x6 = document.querySelector('#x6');
    // $x6.appendChild(stencil.container)
    graph.fromJSON(data);
    document.querySelector('.condition-box').addEventListener('click', (e) => {
      console.log(e)
      if (e.target) {

      }
    })
    graph.centerContent();
  }, [])
  const showEdit = () => {
    data.nodes.push({
      id: 'node3', // String，可选，节点的唯一标识
      x: 240,       // Number，必选，节点位置的 x 值
      y: 40,       // Number，必选，节点位置的 y 值
      width: 100,   // Number，可选，节点大小的 width 值
      height: 100,  // Number，可选，节点大小的 height 值
      shape: 'html',
      extraData: {
        name: '3'
      },
      html: (e) => {
        return renderHtml(e.id)
      },
    })
    graph.fromJSON(data);
  };
  const showExport = () => {
    console.log(graph.toJSON())
  }
  return (
    <div id="x6">
      <Button type="primary" onClick={showEdit}>add</Button>
      <Button type="primary" onClick={showExport}>export</Button>
      <div id="container"></div>
    </div>)
}