import React, { useState } from "react";
import { Row, Col, Tag, Button, Input, AutoComplete, Select, Table, Pagination } from 'antd';
const { Option } = Select;
import { PlusOutlined, EditOutlined, DeleteOutlined, ExportOutlined, ImportOutlined, SearchOutlined } from '@ant-design/icons';
import './index.less';
import NameTableEditPage from "./edit";
import useUserModel from '../../../model/useUserModel';
export default function NameTablePage() {
  const user = useUserModel();
  console.log(user)
  const [selectedRowKeys, setSelectRowKeys] = useState([]);
  const [visible, setVisible] = useState(false);
  const [item, setItem] = useState();
  const onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectRowKeys(selectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const typeMap = {
    0: 'IPv4',
    1: 'IPv6',
    2: '物理地址',
  }
  const sourceMap = {
    1: '控制台',
    2: '中心'
  }
  const columns = [
    {
      title: '类型',
      dataIndex: 'type',
      render: (key) => typeMap[key]
    },
    {
      title: '对象',
      dataIndex: 'address',
      render: (key, data) => { return <span style={{ color: data.color }}>{key}</span> }
    },
    {
      title: '别名',
      dataIndex: 'alias',
    },
    {
      title: '来源',
      dataIndex: 'source',
      render: (key) => sourceMap[key]
    },
    {
      title: '操作',
      dataIndex: '',
      key: 'x',
      render: () => <Button type="link">变更日志</Button>,
    },
  ];
  const colorList = [
    { "value": "magenta" },
    { "value": "red" }, { "value": "volcano" },
    { "value": "orange" }, { "value": "gold" },
    { "value": "lime" }, { "value": "green" },
    { "value": "cyan" }];
  const data = [];
  for (let i = 0; i < 56; i++) {
    data.push({
      key: i,
      type: i % 3,
      address: `London, Park Lane no. ${i}`,
      alias: `alias ${i}`,
      source: i % 2 + 1,
      color: colorList[i % 8].value
    });
  }
  const showEdit = (flag) => {
    if (flag) {
      setItem(data[selectedRowKeys[0]])
    }
    changeVisible(true);
  }
  const changeVisible = (flag) => {
    setVisible(flag);
  }
  const onChange = (page, pageSize) => {
    console.log(page, pageSize);
  }
  const onShowSizeChange = (current, size) => {
    console.log(current, size)
  }
  return (
    <div className="name-table-page">
      <Row>
        <Col span="24">
          <label>对象类型：</label><Tag>IPv4</Tag><Tag>IPv6</Tag><Tag>物理地址</Tag>
        </Col>

        <Col span="24">
          <label>数据来源：</label><Tag>控制台</Tag><Tag>中心</Tag></Col>
      </Row>
      <Row>
        <Col span="12" >
          <Button type="primary" icon={<PlusOutlined />} onClick={showEdit}></Button>
          <Button type="primary" icon={<EditOutlined />} onClick={() => { showEdit(true) }} disabled={selectedRowKeys.length !== 1 ? true : false}></Button>
          <Button type="primary" icon={<DeleteOutlined />} disabled={selectedRowKeys.length === 0 ? true : false}></Button>
          <Button type="primary" icon={<ExportOutlined />}></Button>
          <Button type="primary" icon={<ImportOutlined />}></Button>
        </Col>
        <Col span="12">
          <Input.Group compact className="right-search">
            <Select defaultValue="address" style={{ width: '30%' }}>
              <Option value="address">对象</Option>
              <Option value="alias">别名</Option>
            </Select>
            <AutoComplete
              style={{ width: '70%' }}
              placeholder="按回车进行搜索"
            />
          </Input.Group>
          <Button type="primary" icon={<SearchOutlined />}></Button>
        </Col>
        <Col span="24">
          <Table rowSelection={rowSelection} columns={columns} dataSource={data} bordered pagination={{
            total: data.length,
            showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
            defaultPageSize: 10,
            defaultCurrent: 1,
            onChange: onChange,
            onShowSizeChange: onShowSizeChange,
            pageSizeOptions: ['10', '20', '50', '100']
          }} />
        </Col>
      </Row>
      {visible ? <NameTableEditPage visible={visible} changeVisible={changeVisible} item={item}></NameTableEditPage> : ''}
    </div>
  )
}