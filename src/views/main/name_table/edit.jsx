import React, { useState,useEffect } from 'react';
import { Drawer, Form, Button, Input, Select, Tag } from 'antd';

const { Option } = Select;
const { Search } = Input;
export default function NameTableEditPage(props) {
  const { visible, changeVisible, item } = props;
  let [editForm, setEditForm] = useState(item||{ color: ['red'], alias: '', type: 1, address: '' });
  console.log(editForm)
  // useEffect(() => {
  //   console.log(item)
  //   setEditForm(item || { color: ['red'], alias: '', type: 1, address: '' })
  // })
  let title = item ? '编辑名字表' : '创建名字表'
  const onClose = () => {
    changeVisible(false)
  };
  const onSubmit = () => {
    document.querySelector('#nameTableEdit #submit').click();
  }
  const onFinish = (values) => {
    console.log(values)
  }
  const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 20 },
  };
  const typeList = [{
    key: 'IPv4',
    value: 0
  }, {
    key: 'IPv6',
    value: 1
  }, {
    key: '物理地址',
    value: 2
  }]
  const colorList = [
    { "value": "magenta" },
    { "value": "red" }, { "value": "volcano" },
    { "value": "orange" }, { "value": "gold" },
    { "value": "lime" }, { "value": "green" },
    { "value": "cyan" }];
  const onSearch = () => {

  }
  let color = ['red'];
  const onChange = (value) => {
    console.log(value)
    color = [value[value.length - 1]];
    console.log(color)
  }
  function tagRender(props) {
    const { label, value, closable, onClose } = props;
    const onPreventMouseDown = event => {
      event.preventDefault();
      event.stopPropagation();
    };
    return (
      <Tag
        color={value}
        onMouseDown={onPreventMouseDown}
        closable={closable}
        onClose={onClose}
        style={{ marginRight: 3 }}
      >
        {label}
      </Tag>
    );
  }
  return (
    <>
      <Drawer
        title={title}
        width={400}
        onClose={onClose}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
        footer={
          <div
            style={{
              textAlign: 'center',
            }}
          >
            <Button onClick={onClose} style={{ marginRight: 8 }}>
              关闭
            </Button>
            <Button onClick={onSubmit} type="primary">
              提交
            </Button>
          </div>
        }
      >
        <Form
          {...layout}
          name="nameTableEdit"
          initialValues={editForm}
          onFinish={onFinish}
        >
          <Form.Item
            label="类型"
            name="type"
            rules={[{ required: true, message: '请输入对象!' }]}
          >
            <Select>
              {typeList.map(item => {
                return <Option value={item.value} key={item.value}>{item.key}</Option>
              })}
            </Select>
          </Form.Item>
          <Form.Item
            label="对象"
            name="address"
            rules={[{ required: true, message: '请输入对象!' }]}
          >
            <Search placeholder="请输入对象" onSearch={onSearch} enterButton="解析地址" />
          </Form.Item>
          <Form.Item
            label="别名"
            name="alias"
            rules={[{ required: true, message: '请输入别名!' }]}
          >
            <Search placeholder="请输入别名" onSearch={onSearch} enterButton="解析别名" />
          </Form.Item>
          <Form.Item
            label="颜色"
            name="color"
            rules={[{ required: true, message: '请选择颜色!' }]}>
            <Select
              showArrow
              mode="tags"
              tagRender={tagRender}
              style={{ width: '100%' }}
              options={colorList}
              onChange={onChange}
              value={color}
            />
          </Form.Item>
          <Form.Item>
            <Button id="submit" type="primary" htmlType="submit" block style={{ display: 'none' }}>
              提交
            </Button>
          </Form.Item>
        </Form>
      </Drawer>
    </>
  );
}