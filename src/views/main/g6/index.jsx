import React, { useEffect } from "react";
import G6, { registerBehavior, registerNode } from '@antv/g6';
export default function G6Demo() {
  let graph;
  const data = {
    nodes: [
      {
        id: "node1",
        label: "Circle1",
        x: 150,
        y: 150
      },
      {
        id: "node2",
        label: "Circle2",
        x: 400,
        y: 150,
        getAnchorPoints() {
          return [
            [0, 0.5], // 左侧中间
            [1, 0.5], // 右侧中间
          ];
        },
      },
      {
        id: "node3",
        label: "Circle3",
        type: 'dom-node',
        x: 500,
        y: 100
      }
    ],
    edges: [
      {
        source: "node1",
        target: "node2", style: {
          endArrow: true,
          startArrow: true
        }
      },
      {
        source: "node2",
        target: "node3"
      }
    ]
  };
  registerNode(
    'dom-node',
    {
      draw: (cfg, group) => {
        console.log(cfg)
        return group.addShape('dom', {
          attrs: {
            width: 100,
            height: 100,
            // 传入 DOM 的 html
            html: `
          <div style="background-color: #fff; border: 2px solid #5B8FF9; border-radius: 5px; width: 100px; height: 100px; display: flex;">
            <div style="height: 100%; width: 33%; background-color: #CDDDFD">
              <img alt="img" style="line-height: 100%; padding-top: 6px; padding-left: 8px;" src="https://gw.alipayobjects.com/mdn/rms_f8c6a0/afts/img/A*Q_FQT6nwEC8AAAAAAAAAAABkARQnAQ" width="20" height="20" />  
            </div>
            <span style="margin:auto; padding:auto; color: #5B8FF9">${cfg.label}</span>
          </div>
            `,
          },
          draggable: false,
        });
      },
    },
    'single-node',
  );
  registerBehavior("dice-er-scroll", {
    getDefaultCfg() {
      return {
        multiple: true,
      };
    },
    getEvents() {
      return {
        wheel: "scorll",
        click: "click",
        "node:mousemove": "move",
      };
    },
    move(e) {
      debugger;
      const name = e.shape.get("name");
      const item = e.item;
  
      if (name && name.startsWith("item")) {
        graph.updateItem(item, {
          selectedIndex: Number(name.split("-")[1]),
        });
      } else {
        graph.updateItem(item, {
          selectedIndex: NaN,
        });
      }
    },
  })
  useEffect(() => {
    graph = new G6.Graph({
      renderer: 'svg',
      container: "container",
      width: 1000,
      height: 500,
      modes: {
        default: ['dice-er-scroll','drag-canvas', 'zoom-canvas', 'drag-node'], // 允许拖拽画布、放缩画布、拖拽节点
      },
      defaultNode: {
        shape: "circle",
        size: [100],
        color: "#5B8FF9",
        style: {
          fill: "#9EC9FF",
          lineWidth: 3
        },
        labelCfg: {
          style: {
            fill: "#fff",
            fontSize: 20
          }
        }
      },
      defaultEdge: {
        style: {
          stroke: "#e2e2e2"
        }
      }
    });

    graph.data(data);
    graph.render();
  }, [])

  return (
    <div id="x6">
      <div id="container"></div>
    </div>)
}