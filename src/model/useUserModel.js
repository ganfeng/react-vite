import { useAsync } from "@umijs/hooks";
import { login as loginService } from "../service/user";
import { useState } from "react";
import { createModel } from "hox";


function useUser() {
  const [userInfo, setUserInfo] = useState();

  const { run, loading } = useAsync(loginService, [], {
    manual: true
  });

  const login = (username) => {
    run(username).then(data => {
      setUserInfo(data);
    });
  };

  const logout = () => {
    setUserInfo(undefined);
  };

  return {
    userInfo,
    login,
    loading,
    logout
  };
}

export default createModel(useUser);
